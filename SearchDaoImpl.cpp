#include "SearchDAO.h"

MYSQL* SearchDAO::connection_handle;
string SearchDAO::prepared_query;
MYSQL_RES* SearchDAO::results;

void SearchDAO::get_candidates(string skill) {
	// form the query - "1. Given a skill, present a list of all candidates ( candidate_id, candidate_name, company ) who have that skill"
	prepared_query = "SELECT c.candidate_id, c.candidate_name, c.company FROM Candidate c INNER JOIN CandidateSkill s ON c.candidate_id = s.candidate_id AND s.skill = '" + skill + "';";
	execute_query();
}

void SearchDAO::get_skills(string candidate_name) {
	// form the query - "Given a candidate_name, find all the skills for that candidate."
	prepared_query = "SELECT c.candidate_id, c.candidate_name, s.skill FROM CandidateSkill s INNER JOIN Candidate c ON s.candidate_id = c.candidate_id AND c.candidate_name = '" + candidate_name + "';";
	execute_query();
}

void SearchDAO::establish_connection() {
    // create a mysql instance
    connection_handle = mysql_init(NULL);
 
    // connect to the database with the credentials required
    if (!mysql_real_connect(connection_handle, host, user, NULL, db, 0, NULL, 0)) {
      printf("Conection error : %s\n", mysql_error(connection_handle));
      exit(1);
    }
}

// establish a connection and execute a query
void SearchDAO::execute_query() {
	establish_connection();

	char *sql_query = new char[prepared_query.length() + 1];
	strcpy(sql_query, prepared_query.c_str());
	printf("\nRunning query: %s\n", sql_query);

	if (mysql_query(connection_handle, sql_query))
	{
		printf("Error performing MySQL query: %s\n", mysql_error(connection_handle));
		exit(1);
	}

	results = mysql_store_result(connection_handle);
	delete[] sql_query;
	mysql_close(connection_handle);
}