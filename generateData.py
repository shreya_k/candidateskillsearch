import random

firstNames = [line.strip() for line in open("./first_names.txt", 'r')]
lastNames = [line.strip() for line in open("./last_names.txt", 'r')]
skills = [line.strip() for line in open("./skills.txt", 'r')]
companyNames = [line.strip() for line in open("./company_names.txt", 'r')]

# form full names from first name and last name combinations
f = open('./full_names.txt', 'w')
fullNames = []
for i in range(1,1000001):
	name = firstNames[random.randint(0, 5162)] + " " + lastNames [random.randint(0, 88798)]
	fullNames.append(name)
	f.write(name + "\n")

# generate candidate_ids 
candidate_ids = []
for i in range(1,1000001):
	candidate_ids.append(i)

# Form insert statement for Candidate table
candidate_values = 'INSERT INTO Candidate (candidate_id,candidate_name,company,salary) VALUES (%d, "%s", "%s", %d)' %(candidate_ids[0], fullNames[random.randint(0,999999)], companyNames[random.randint(0, 946)], random.randint(20000, 20000000))
for i in range(1,1000000):
	candidate_values += ',(%d, "%s", "%s", %d)' %(candidate_ids[i], fullNames[random.randint(0,999999)], companyNames[random.randint(0, 946)], random.randint(20000, 20000000))
candidate_values += ";"

f = open("./candidate_insert_script.sql", 'w');
f.write(candidate_values)

# Form insert statement for CandidateSkill table
id_skill_set = set()
i = random.randint(0,999999)
j = random.randint(0,69)
candidate_skill_values = 'INSERT INTO CandidateSkill (candidate_id,skill) VALUES (%d, "%s")' %(candidate_ids[i], skills[j])
num = 1;
while (num<10000000):
	i = random.randint(0,999999)
	j = random.randint(0,69)
	if((i,j) not in id_skill_set):
		candidate_skill_values += ',(%d, "%s")' %(candidate_ids[i], skills[j])
		id_skill_set.add((i, j))
		num += 1
	else:
		continue

candidate_skill_values += ';'
f = open("./candidate_skill_insert_script.sql", 'w');
f.write(candidate_skill_values)