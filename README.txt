Assumptions:
1. Candidate name used for searching is the full name (First Name + Last Name) of the candidate. Candidate Names are not unique, multiple candidates have the same full name.
2. Search based on skill will be on the exact skill string, pattern matching is not supported. For example, searching for 'C' will only return candidates who have 'C' as a skill, those with 'C++', but not 'C' will not show up in search results.

DB Schema:

DB Name: JobPortal

CREATE TABLE Candidate (
  id int(15) NOT NULL AUTO_INCREMENT,
  candidate_id int(15) NOT NULL,
  candidate_name varchar(100) NOT NULL,
  company varchar(100) default NULL,
  salary int(15) default 0,
  PRIMARY KEY (id),
  UNIQUE KEY (candidate_id),
  INDEX (candidate_name)
) AUTO_INCREMENT=1;


CREATE TABLE CandidateSkill(
   candidate_id int(15) NOT NULL,
   skill varchar(100) NOT NULL,
   Primary Key (candidate_id, skill), 
   CONSTRAINT Foreign Key (candidate_id) REFERENCES Candidate(candidate_id),
   INDEX (skill),
   INDEX (candidate_id)
); 

DB Queries:
1.  Given a skill, present a list of all candidates ( candidate_id, candidate_name, company ) who have that skill.
SELECT s.skill FROM CandidateSkill s
INNER JOIN Candidate c ON s.candidate_id = c.candidate_id AND c.candidate_name='ROSELEE AHALT';

2.  Given a candidate_name, find all the skills for that candidate.
SELECT c.* FROM Candidate c
INNER JOIN CandidateSkill s ON c.candidate_id = s.candidate_id AND s.skill = 'Python';

Join was preferred over a sub-query as it offers superior performance

Test DataSet:

The script 'generateData.py' was used to generate scripts for inserting data in both the tables.
Large datasets of first name, last name, company name were obtained online and a small dataset of skills was created manually as input to generateData.py.
The Candidate table has 1 million rows and the CandidateSkill table has 10 million rows.

Config parameters:
Config parameters like host, username, password and database name have been added in the file 'Constants.h'.

Importing the Database:
To import the database, start your MySQL server, login to MySQL shell and create a new blank database to serve as a destination for the data:
1) CREATE DATABASE JobPortal;

2) As we need to insert a large amount of data, set the comunnication buffer 
SET GLOBAL max_allowed_packet=1073741824;

Then log out of the MySQL shell and type the following on the command line:
2) mysql -u root JobPortal < JobPortal.sql

Running the queries:
Once the database is setup, compile and run the following:
3) g++ -o CandidateSkillSearch $(mysql_config --cflags) CandidateSkillSearch.cpp SearchDaoImpl.cpp $(mysql_config --libs)
4)./CandidateSkillSearch
5) Enter "1" or "2" to query by candidate name and skill respectively and press any other key to exit.

Example:
skedia@LM-BLR-00668683:~/Documents/JobPortal|master⚡
⇒  g++ -o CandidateSkillSearch $(mysql_config --cflags) CandidateSkillSearch.cpp SearchDaoImpl.cpp $(mysql_config --libs)
skedia@LM-BLR-00668683:~/Documents/JobPortal|master⚡
⇒  ./CandidateSkillSearch
Enter option number:
1 - get skills by candidate name
2 - get candidates by skill
Any other key to exit
1
Enter candidate's full name:
helen dungy

Running query: SELECT c.candidate_id, c.candidate_name, s.skill FROM CandidateSkill s INNER JOIN Candidate c ON s.candidate_id = c.candidate_id AND c.candidate_name = 'helen dungy';
Query Results:
629911     HELEN DUNGY     Equity Valuation
629911     HELEN DUNGY     Git
629911     HELEN DUNGY     Hadoop
629911     HELEN DUNGY     JavaScript
629911     HELEN DUNGY     Kafka
629911     HELEN DUNGY     Machine Learning
629911     HELEN DUNGY     Market Research
629911     HELEN DUNGY     Microsoft Office
629911     HELEN DUNGY     node.js
629911     HELEN DUNGY     Product Management
629911     HELEN DUNGY     Search Engine Optimization
629911     HELEN DUNGY     Social Media Marketing
999149     HELEN DUNGY     AJAX
999149     HELEN DUNGY     CSS
999149     HELEN DUNGY     ElasticSearch
999149     HELEN DUNGY     Hadoop
999149     HELEN DUNGY     HTML
999149     HELEN DUNGY     Maven
999149     HELEN DUNGY     Product Management
999149     HELEN DUNGY     Unix
999149     HELEN DUNGY     Verilog

Query returned 21 rows