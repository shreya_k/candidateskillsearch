#include <mysql.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include "Constants.h"

using namespace std;

class SearchDAO {
	static MYSQL *connection_handle;
	static string prepared_query;

	public:
		static MYSQL_RES *results;

	public:
		SearchDAO() {}
		void get_candidates(string);
		void get_skills(string);

	private:
		static void establish_connection();
		static void execute_query();
};