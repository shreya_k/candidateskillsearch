#include "SearchDAO.h"

class CandidateSkillSearch {
	public:
		void get_skills_by_candidate_name();
		void get_candidates_by_skill();
	private:	
		void internal_get_skills(string);
		void internal_get_candidates(string);
		void display_results(MYSQL_RES *);
};
 
