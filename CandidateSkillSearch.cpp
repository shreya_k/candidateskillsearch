#include "CandidateSkillSearch.h"

// get skills for a candidate name entered as input
void CandidateSkillSearch::get_skills_by_candidate_name() {
	printf("Enter candidate's full name:\n");
	cin.ignore(INT_MAX, '\n');

	string candidate_name;
	if(getline(cin, candidate_name)) {

		if(candidate_name.empty()) {
			printf("Error: Candidate name is empty\n");
			exit(1);
		}
		internal_get_skills(candidate_name);
	}
	else
		throw std::runtime_error("getline failed");  
}

// get candidates for a skill entered as input
void CandidateSkillSearch::get_candidates_by_skill() {
	printf("Enter skill:\n");
	cin.ignore(INT_MAX, '\n');

	string skill;
	if(getline(cin, skill)) {

		if(skill.empty()) {
			printf("Error: Skill is empty\n");
			exit(1);
		}
		internal_get_candidates(skill);
	}
	else
		throw std::runtime_error("getline failed");  
}

// call the DAO function to get candidate skills
void CandidateSkillSearch::internal_get_skills(string candidate_name) {
	SearchDAO *search_dao = new SearchDAO();
	search_dao->get_skills(candidate_name);
	MYSQL_RES *results = search_dao->results;
	display_results(results);
	mysql_free_result(results);
}

// call the DAO fucntion to get candidate details
void CandidateSkillSearch::internal_get_candidates(string skill) {
	SearchDAO *search_dao = new SearchDAO();
	search_dao->get_candidates(skill);
	MYSQL_RES *results = search_dao->results;
	display_results(results);
	mysql_free_result(results);
}

// print each output row of the query result
void CandidateSkillSearch::display_results(MYSQL_RES *results) {
	int row_count = mysql_num_rows(results);
	int num_fields = mysql_num_fields(results);
	int i;

	if(row_count > 0) {
		printf("Query Results:\n");

		MYSQL_ROW row;  // each result row 
	    while ((row = mysql_fetch_row(results)) != NULL) {
	    	for(i = 0; i < num_fields; i++)
	    	{
	    		printf("%s     ", row[i] ? row[i] : "NULL");
	    	}
	    	printf("\n");
	    }
	    printf("\nQuery returned %d rows\n\n", row_count);

	} else {
		printf("No rows returned\n\n");
	}
}

int main() { 
	int option = 0;
	CandidateSkillSearch *candidate_skill_search = new CandidateSkillSearch();
	while(1) {
		printf("Enter option number:\n");
		printf("1 - get skills by candidate name\n");
		printf("2 - get candidates by skill\n");
		printf("Any other key to exit\n");
		cin >> option;
		if(option == 1){
			candidate_skill_search->get_skills_by_candidate_name();
		} else if(option == 2) {
			candidate_skill_search->get_candidates_by_skill();
		} else {
			break;
		} 
	}
	return 0;
}
